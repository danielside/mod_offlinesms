local libtelegram = {}
local st = require "util.stanza"

local telegramconfig = require("telegram");
local telegram_config = telegramconfig.telegram_config;
local xmpp2tg_address = telegramconfig.xmpp2tg_address;

-- a telegram user must be encoded onto the text message
-- example: "[Test] Hola" is the message "Hola" to user "Test"
libtelegram.parse_telegram_user = function(message)
   if (message == nil) then
      return nil, nil
   end

   local first_bracket = string.find(message, "[", 1, true)
   local second_bracket = string.find(message, "]", 1, true)
   local tg_user = nil
   local real_message = nil

   if (first_bracket == 1 and second_bracket ~= nil) then
      tg_user = string.sub(message, first_bracket + 1, second_bracket - 1)
      tg_user = string.gsub(tg_user, "%s+", "")
      real_message = string.gsub(string.sub(message, second_bracket + 1), "^%s+", "")
   end
   return tg_user, real_message
end

libtelegram.parse_xmpp_user = function(fqn)
  local first_at = string.find(fqn, "[", 1, true)
end

-- sends the message using the telegram-cli instance that should be running on port configured
libtelegram.send_telegram_message = function(user, message)
    local telegram_command = "msg " .. user .. " " .. message
    local send_command = "/bin/echo \"" .. telegram_command .. "\" | nc " .. telegram_config.telegram_daemon_server  .. " " .. telegram_config.telegram_daemon_port .. " -q 1"
    os.execute(send_command)
end

libtelegram.send_telegram_photo = function(user, photo_path, photo_file_name, module)
   local path_in_container = telegram_config.telegram_daemon_uploads_path .. photo_file_name
   local scp_command = "/usr/bin/scp " .. photo_path .. " telegramd@" .. telegram_config.telegram_daemon_server .. ":" .. path_in_container
   local telegram_command = "send_photo " .. user .. " '" .. path_in_container .. "'"
   local send_command = "/bin/echo \"" .. telegram_command .. "\" | nc " .. telegram_config.telegram_daemon_server .. " " .. telegram_config.telegram_daemon_port .. " -q 1"
   
   os.execute(scp_command)
   os.execute(send_command)
end

libtelegram.translate_telegram_user = function(xmpp_user)
   local found = nil
   for k,v in pairs(xmpp2tg_address) do
      if k == xmpp_user then
          found = v
          break
      end
   end
   return found
end

libtelegram.on_telegram_message = function(event,tg_user,offline_module)
  local stanza = st.deserialize(event.stanza);
  local stanza_to = stanza.attr.to
  local message = nil

  -- photo?
  local attached_upload = libtelegram.get_attached_upload(stanza)
  local message_body = stanza:get_child_text("body")

  if attached_upload ~= nil then
     local photo_file_name = string.sub(attached_upload, libtelegram.strrpos(attached_upload, "/") + 1)
     libtelegram.send_telegram_photo(tg_user, attached_upload, photo_file_name, offline_module)
  else
    if message_body ~= nil then
      libtelegram.send_telegram_message(tg_user, message_body)
    end
  end
end

-- finds a url for a http uploaded file URL among the stanza tags
-- and returns its path in the filesystem
libtelegram.get_attached_upload = function(stanza)
  local photo_url = nil
  local photo_path = nil
  local path_dir = telegram_config.uploads_path_prefix
  local path_prefix = telegram_config.uploads_path_fragment 
  
  for k,tag in pairs(stanza.tags) do
    local possible_url = tag[1]

    if possible_url ~= nil and type(possible_url) == "string"  then
      local begin_url = string.find(possible_url, path_prefix, 1, true)
      if begin_url ~= nil then
        photo_path = string.sub(possible_url, begin_url)
        if photo_path ~= nil then
           photo_path = path_dir .. photo_path
        end
      end
    end
  end 
     
  return photo_path
end

libtelegram.strrpos = function(s, p)
  if (s == nil) then
    return nil
  end

  local init = 1
  local starts = 0
  local ends = nil

  local itera = 0
  while starts ~= nil do
     starts = string.find(s, p, init + 1, true)
     if starts ~= nil then
       init = starts
     end
     itera = itera + 1
  end
  return init
end

libtelegram.urlfilename = function (url)
  local pos_last_dash = libtelegram.strrpos(url, "/")
  if (pos_last_dash ~= nil) then
   return string.sub(url, pos_last_dash + 1)
  end
  return nil
end


return libtelegram;
