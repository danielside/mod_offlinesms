-- Prosody IM
-- 
-- Copyright (C) 2019 Danielside
--
--

local datetime = require "util.datetime";
local jid_split = require "util.jid".split;
local st = require "util.stanza"

local offline_messages = module:open_store("offline", "archive");
local mensajes_notificables = module:open_store("mensajes_notificables");

module:add_feature("msgoffline");

--CONFIG
-- Symlink offline_sms.lua from /usr/lib/prosody
offline_sms = require("offlinesms");
libtelegram = require("libtelegram");

--/CONFIG

local libtelegram = libtelegram;

prosody.unlock_globals()
require "ltn12";
prosody.lock_globals()

local socket = require 'socket'
local smtp = require 'socket.smtp'

local offline_config = offline_sms.offline_config;
local allowed_for_sms = offline_sms.allowed_for_sms;
local telegram_groups_allowed_for_sms = offline_sms.telegram_groups_allowed_for_sms;
local contact_info = offline_sms.contact_info;

local function imprimir_tabla(t)
  local debug = "";
  for k,v in pairs(t) do
    debug = debug .. ", " .. v
  end
end

local function hay_mensajes_previos_de(t, usuario)
  local hay = false;
  for k,v in pairs(t) do
    if (v == usuario) then
      hay = true;
    end
  end
  return hay;
end

function informacion_de_contacto(usuario)
   local found = nil
   for k,v in pairs(contact_info) do
      if k == usuario then
          found = v
          break
      end
   end
   return found
end

local function mail_to(from,to,subject_text,body_text)

   local mail_rcpt = {
             "<" .. to .. ">" }

   local mesgt = {
         headers = {
            from = "XMPP " .. from,
            to = mail_rcpt[1],
            subject = subject_text}, body = body_text}

   local r,e = smtp.send{
           from = from,
           rcpt = mail_rcpt,
           source = smtp.message(mesgt),
           server = offline_config.mail_server,
           port = offline_config.mail_server_port,
           user = offline_config.mail_server_user,
           password = offline_config.mail_server_password,
   }

end

function allowed_to_send_sms(user, host, message_body)
   if (allowed_for_sms == nil) then
      return false
   end

   local allowed = false
   for key, jid in pairs(allowed_for_sms) do
      local a_user, a_host = jid_split(jid)
      if (a_user ~= nil and a_host ~= nil) then
         --string.find covers the case when host is in the form server/resource
         if (a_user == user and string.find(host, a_host, 1, true)) then
            allowed = true
            break
         end
      end
   end

   -- the special case for telegram, all telegram users allowed and no groups allowed
   -- except for the ones explicitly allowed
   if user == "telegram" then

    if string.find(message_body, offline_config.telegram_msg_group_delim, 1, true) then
      allowed = false
      for key, gid in pairs(offline_config.telegram_groups_allowed_for_sms) do
        if string.find(message_body, offline_config.telegram_msg_group_delim .. " " .. gid, 1, true) then

         allowed = true
         break
       end
      end
    end
   end

   return allowed
end

local function notify (event)
   local session, stanza = event.origin, event.stanza;

   local to = stanza.attr.to;
   local username_to, host_to = jid_split(to);

   local from = stanza.attr.from;
   local username_from, host_from = jid_split(from);

   if to then
     local contacto = informacion_de_contacto(username_to);
     if (contacto) then
	 local message_body = stanza:get_child_text("body");
	 local mail_from = offline_config.mail_from_address;
	 local from_jid = tostring(from);
	 local subject = offline_config.mail_subject .. from_jid;
         local message = "[" .. username_from .. "] " .. message_body
	 
         -- email
	 local email_to = contacto[1];
	 if offline_config.notify_email == 1 and email_to then
	    mail_to(mail_from,email_to,subject,message)
	 end
	 
         -- sms
         local telefono = contacto[2];	 
	 if offline_config.notify_sms == 1 and telefono then
	    if allowed_to_send_sms(username_from, host_from, message_body) then
	       if offline_config.notify_fake_sms == 0 then
		  local sms_email_to = telefono .. offline_config.mail_to_sms_domain
		  mail_to(mail_from,sms_email_to,subject,message)
	       else
		  if email_to then
		     mail_to(mail_from,email_to,subject,message .. " (SMS fake)")
		  else
		     module:log("info", "SMS fake a " .. sms_email_to)
		  end
	       end
	   end
	 end
	 
     end
   end
end

module:hook("message/offline/handle", function(event)
	local origin, stanza = event.origin, event.stanza;
	local to = stanza.attr.to;
	local node;
	if to then
		node = jid_split(to)
	else
		node = origin.username;
	end

        local message_body = stanza:get_child_text("body");
        local is_text_stanza = true
        if message_body == nil or string.len(message_body) == 0 then
           is_text_stanza = false;
        end

        if is_text_stanza and to then
	   -- tratamiento diferenciado de mensajes telegram
	   local tg_user = libtelegram.translate_telegram_user(to)
	   if tg_user ~= nil then
	      -- no sigue la función, no guarda los mensajes offline
	      libtelegram.on_telegram_message(event, tg_user, module)
	      return true;
	   end

          -- obtener mensajes offline para el usuario destino
	  local mensajes_offline = mensajes_notificables:get(node);
	  
	  if mensajes_offline == nil then
	    mensajes_offline = {};
	  else
	    -- imprimir_tabla(mensajes_offline);
	  end

          -- si no hay mensajes previos del usuario origen, se inserta y notifica
	  if (not hay_mensajes_previos_de(mensajes_offline, origin.username)) then
	    --module:log("info", "insertando mensajes en el almacén de " .. node);
  	    table.insert(mensajes_offline, origin.username);
	    notify(event);
	  end
	  
	  
	  mensajes_notificables:set(node, mensajes_offline);
	end

	return offline_messages:append(node, nil, stanza, os.time(), "");
end, -1);

module:hook("message/offline/broadcast", function(event)
	local origin = event.origin;

	local node, host = origin.username, origin.host;

	local data = offline_messages:find(node);
	if not data then return true; end
	for _, stanza, when in data do
		stanza:tag("delay", {xmlns = "urn:xmpp:delay", from = host, stamp = datetime.datetime(when)}):up(); -- XEP-0203
		origin.send(stanza);
	end
	offline_messages:delete(node);
	
        --module:log("info", "poner a nil almacén de " ..  node);
        mensajes_notificables:set(node, nil);
	
	return true;
end, -1);
